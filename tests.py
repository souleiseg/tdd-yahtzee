import unittest
from yahtzee import Yahtzee


class TestYahtzee(unittest.TestCase):
    def test_ones(self):
        d_list_1 = [1, 1, 1, 2, 3]
        assert Yahtzee.ones(d_list_1) == 3

    def test_twos(self):
        d_list_2 = [2, 1, 1, 2, 3]
        assert Yahtzee.twos(d_list_2) == 4

    def test_threes(self):
        d_list_3 = [3, 4, 6, 2, 3]
        assert Yahtzee.threes(d_list_3) == 6

    def test_fours(self):
        d_list_4 = [4, 4, 5, 2, 4]
        assert Yahtzee.fours(d_list_4) == 12

    def test_fives(self):
        d_list_5 = [5, 1, 5, 5, 5]
        assert Yahtzee.fives(d_list_5) == 20

    def test_sixes(self):
        d_list_6 = [6, 5, 4, 6, 6]
        assert Yahtzee.sixes(d_list_6) == 18

    def test_chance(self):
        d_list_chance = [6, 2, 6, 6, 3]
        sum = 0
        for i in d_list_chance:
            sum += i
        assert Yahtzee.chance(d_list_chance) == 23

    def test_yahtzee(self):
        d_list_yahtzee = [2, 2, 2, 2, 2]
        assert Yahtzee.yahtzee(d_list_yahtzee) == 50

    def test_three_of_a_kind(self):
        d_list_three_oak = [2, 2, 2, 4, 1]
        assert Yahtzee.three_of_a_kind(d_list_three_oak) == sum(d_list_three_oak)

    def test_four_of_a_kind(self):
        d_list_four_oak = [5, 5, 5, 5, 6]
        assert Yahtzee.four_of_a_kind(d_list_four_oak) == sum(d_list_four_oak)

    def test_fullhouse(self):
        d_list_fullhouse = [2, 2, 2, 6, 6]
        assert Yahtzee.fullhouse(d_list_fullhouse) == 25

    def test_smallstraight(self):
        d_list_smallstraight = [1, 2, 3, 4, 6]
        assert Yahtzee.smallstraight(d_list_smallstraight) == 30

    def test_largestraight(self):
        d_list_largestraight = [2, 3, 4, 5, 6]
        assert Yahtzee.largestraight(d_list_largestraight) == 40
