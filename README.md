# TDD-Yahtzee

## Cours
Tests unitaires - TP1

## Membres du groupe
Souleimane SEGHIR

### Fonctionnement du jeu
Le jeu se déroule depuis le fichier game.py où le joueur peu faire un total de 3 lancers, il dispose initialement de 5 dés.  
A chaque lancers des dés peuvent être mis de côté, les suivants sont relancés au tours suivant.  
Le joueur constitue ainsi sa liste de dés au travers de chaque lancé.  
Tout les dés restant après le 3 ème lancer sont automatiquement ajoutés à la liste des dés du joueur.  
Une fois son tour terminé, le joueur choisis la combinaison qu'il trouve la plus avantageuse afin de recevoir un maximum de points.  

## Fichiers
#### yahtzee.py
Contient toutes les fonctions de combinaisons testées.

#### main.py
Ce fichier contient le code du moteur de jeu.

#### tests.py
Les tests réalisés se font à partir de ce fichier.

#### Comment jouer ?
A chacun des 3 lancers, la liste des dés qui sont retombés est affichés.
Le joueur doit saisir un nombre compris dans la liste affichée, il peut ne rien saisir s'il ne souhaite pas garder les dés.  
Cette sélection est à réaliser 5 fois.

Une fois les 3 tours passés, les options de combinaisons sont affichées, le joueur peut choisir la plus avantageuse en saisissant un nombre comprise entre 1 et 13 inclus.
