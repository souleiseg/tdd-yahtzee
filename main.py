from yahtzee import Yahtzee
import random

def game():
    dices_in_hand = 5
    saved_dices = []
    tries = 0
    combinations = ['[1]Ones - [2]Twos - [3]Threes - [4]Fours - [5]Fives - [6]Sixes', '[7]Three of a kind - [8]Four of a kind - [9]Full House', '[10]Small Straight - [11]Large Straight - [12]Chance - [13]Yahtzee']

    while tries <= 2:
        tries += 1
        print("Try {}/3 \n".format(tries))
        print("You throw the dices : ")
        actual_dices = []

        for i in range(0, dices_in_hand):
            random_dice = random.randint(1, 6)
            actual_dices.append(random_dice)
        print("Throw result : " + str(actual_dices) + "\n")

        for i in range(0, len(actual_dices)):
            print("Select dice N°{} (leave empty if none)".format(i+1))
            selected_dice = input()
            if selected_dice != '':
                dices_in_hand -= 1
                saved_dices += selected_dice
            if selected_dice == '':
                continue

        if len(saved_dices) != 5 and tries == 3:
            for dice in actual_dices:
                saved_dices.append(dice)
        saved_dices = [int(dice) for dice in saved_dices]
        print("Your saved_dices : {}".format(saved_dices))

    print('Choose your desired combination :')
    for combination in combinations:
        print(combination)
    combination_choice = int(input())

    if combination_choice not in range(1, 14):
        while True:
            print("Please select a value between 1 and 13 to choose your combination")
            combination_choice = int(input())
            if combination_choice in range(1, 14):
                break
    if combination_choice == 1:
        Yahtzee.ones(saved_dices)
    elif combination_choice == 2:
        Yahtzee.twos(saved_dices)
    elif combination_choice == 3:
        Yahtzee.threes(saved_dices)
    elif combination_choice == 4:
        Yahtzee.fours(saved_dices)
    elif combination_choice == 5:
        Yahtzee.fives(saved_dices)
    elif combination_choice == 6:
        Yahtzee.sixes(saved_dices)
    elif combination_choice == 7:
        Yahtzee.three_of_a_kind(saved_dices)
    elif combination_choice == 8:
        Yahtzee.four_of_a_kind(saved_dices)
    elif combination_choice == 9:
        Yahtzee.fullhouse(saved_dices)
    elif combination_choice == 10:
        Yahtzee.smallstraight(saved_dices)
    elif combination_choice == 11:
        Yahtzee.largestraight(saved_dices)
    elif combination_choice == 12:
        Yahtzee.chance(saved_dices)
    elif combination_choice == 13:
        Yahtzee.yahtzee(saved_dices)
    print('Your turn is over.')

if __name__ == '__main__':
    game()