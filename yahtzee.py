class Yahtzee:
    def ones(dice_list):
        score = 0
        for i in (dice_list):
            if i == 1:
                score += 1
        print("Your score is {}".format(score))
        return score

    def twos(dice_list):
        score = 0
        for i in (dice_list):
            if i == 2:
                score += 2
        print("Your score is {}".format(score))
        return score

    def threes(dice_list):
        score = 0
        for i in (dice_list):
            if i == 3:
                score += 3
        print("Your score is {}".format(score))
        return score

    def fours(dice_list):
        score = 0
        for i in (dice_list):
            if i == 4:
                score += 4
        print("Your score is {}".format(score))
        return score

    def fives(dice_list):
        score = 0
        for i in (dice_list):
            if i == 5:
                score += 5
        print("Your score is {}".format(score))
        return score

    def sixes(dice_list):
        score = 0
        for i in (dice_list):
            if i == 6:
                score += 6
        return score

    def chance(dice_list):
        score = 0
        for i in (dice_list):
            score += i
        print("Your score is {}".format(score))
        return score

    def yahtzee(dice_list):
        first_dice = dice_list[0]
        for dice in dice_list:
            if first_dice == dice:
                score = 50
            else:
                score = 0
        print("Your score is {}".format(score))
        return score

    def three_of_a_kind(dice_list):
        counter = 0
        first_dice = dice_list[0]
        score = 0
        for dice in dice_list:
            if first_dice == dice:
                counter += 1
            if counter == 3:
                score = sum(dice_list)
            else:
                continue
        print("Your score is {}".format(score))
        return score

    def four_of_a_kind(dice_list):
        counter = 0
        first_dice = dice_list[0]
        score = 0
        for dice in dice_list:
            if first_dice == dice:
                counter += 1
            if counter == 4:
                score = sum(dice_list)
            else:
                continue
        print("Your score is {}".format(score))
        return score

    def fullhouse(dice_list):
        counter = 0
        score = 0
        dice_list.sort()
        first_dice = dice_list[0]
        for dice in dice_list:
            if first_dice == dice:
                counter += 1
            if counter == 3: #Dans le cas où on a : 2 2 2 6 6 par exemple
                first_dice = dice_list[3]
                for dice in dice_list:
                    if first_dice == dice:
                        counter += 1
                    if counter == 5:
                        score = 25
                    else:
                        continue
            if counter == 2: #Dans le cas où on a : 2 2 6 6 6 par exemple
                first_dice = dice_list[2]
                for dice in dice_list:
                    if first_dice == dice:
                        counter += 1
                    if counter == 5:
                        score = 25
                    else:
                        continue
        print("Your score is {}".format(score))
        return score

    def smallstraight(dice_list):
        score = 0
        counter = 0
        dice_list.sort()
        for dice in range(1, 4):
            if dice_list[dice]-1 == dice_list[dice-1]:
                counter += 1
            if counter == 3:
                score = 30
        print("Your score is {}".format(score))
        return score

    def largestraight(dice_list):
        score = 0
        counter = 0
        dice_list.sort()
        for dice in range(1, 5):
            if dice_list[dice]-1 == dice_list[dice-1]:
                counter += 1
            if counter == 4:
                score = 40
        print("Your score is {}".format(score))
        return score